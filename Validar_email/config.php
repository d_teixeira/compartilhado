<?php 

/**
 * @Author: Biro
 * @Date:   2016-11-03 11:22:53
 * @Last Modified by:   Biro
 * @Last Modified time: 2016-11-09 16:34:59
 */

set_time_limit(10000);

$files_in_dir = scandir("./");

function check_ext( $file ){
	$check_ext = pathinfo( $file );
	return $check_ext['extension'];
}

if ($_SERVER["REQUEST_METHOD"] == "POST"):

$get_file = strip_tags( $_POST['fileName'] );
$open_file = file( $get_file );

$fp = fopen("Base_Full_Clean.csv", 'w');

$finish = "";

foreach ($open_file as $op => $ov) {
	# code...
	list( $email, $nome, $dt_atualizacao, $dt_cadastro, $cidade, $estado, $sexo, $dt_nascimento, $origem, $valido ) = explode(";", $ov);
	//list( $nome, $email, $data, $origem, $temp_msg ) = explode(";", $ov);
	//list( $nome, $email, $data, $temp_msg ) = explode(";", $ov);

	if( filter_var( $email, FILTER_VALIDATE_EMAIL ) ):
		list( $alias, $domain ) = explode("@", $email);
		if( checkdnsrr( $domain ) ):
			$temp_msg = "valido";
			$temp_val = array( $email, $nome, $dt_atualizacao, $dt_cadastro, $cidade, $estado, $sexo, $dt_nascimento, $origem, $temp_msg );
			fwrite($fp, implode(";", $temp_val)."\n");
		else:
			$temp_msg = "dominio invalido";
			$temp_val = array( $email, $nome, $dt_atualizacao, $dt_cadastro, $cidade, $estado, $sexo, $dt_nascimento, $origem, $temp_msg );
			fwrite($fp, implode(";", $temp_val)."\n");
		endif;
	else:
		$temp_msg = "email invalido";
		$temp_val = array( $email, $nome, $dt_atualizacao, $dt_cadastro, $cidade, $estado, $sexo, $dt_nascimento, $origem, $temp_msg );
		fwrite($fp, implode(";", $temp_val)."\n");
	endif;

}

fclose($fp);

endif;


?>