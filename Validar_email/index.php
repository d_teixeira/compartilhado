<?php include 'config.php ' ?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Email</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<div class="container-fluid">
			<div class="row">
			 	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			 		<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" name="scanFile" role="form" enctype="multipart/form-data">
			 			<legend>Verificação de emails</legend>
			 		
			 			<div class="form-group">
			 				<label for="arquivo">Selecione o arquivo que deseja tratar:</label><br>
							<select name="fileName" id="fileName" class="form-control">
								<?php  			
									foreach ($files_in_dir as $fid => $vid) {
										# code...
										if( check_ext( $vid ) === "csv" ):
											echo "<option value='$vid'>$vid</option>";
										endif;
									}
								?>
							</select>				
			 			</div>
			 		 		
			 			<button type="submit" class="btn btn-primary">Processar</button>
			 		</form>
			 	</div>
			 </div>

		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="Hello World"></script>
	</body>
</html>